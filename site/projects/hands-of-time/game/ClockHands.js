ClockHands = function(x, y) {
    // Hand 1
    this.hand1 = game.add.sprite(x, y, 'hand1');
    this.hand1.anchor.setTo(0.5, 0);
    this.hand1.rotation = Math.PI;
    this.hand1_nextPiece = undefined;

    // Hand 2
    this.hand2 = game.add.sprite(x, y, 'hand2');
    this.hand2.anchor.setTo(0.5, 0);
    this.hand2.rotation = Math.PI;
    this.hand2_nextPiece = undefined;

    this.firstMove = false;
    this.secondMove = false;
}

ClockHands.prototype.moveTo = function(piece) {
    this.hand1_nextPiece = piece;
    this.hand2_nextPiece = piece;

    this.firstMove = true;
}

ClockHands.prototype.update = function() {
    if(this.firstMove) {
        if(Math.floor(this.hand1.angle) === this.hand1_nextPiece.angle && Math.floor(this.hand2.angle) === this.hand2_nextPiece.angle) {
            var curBoard = this.hand1_nextPiece.board;

            var hand1NextMove = this.hand1_nextPiece.position + parseInt(this.hand1_nextPiece.number);
            var hand2NextMove = this.hand2_nextPiece.position - parseInt(this.hand2_nextPiece.number);

            if(hand1NextMove >= curBoard.length)
                hand1NextMove = hand1NextMove - curBoard.length;

            if(hand2NextMove < 0)
                hand2NextMove = curBoard.length - Math.abs(hand2NextMove);

            this.hand1_nextPiece.number = '0';
            this.hand2_nextPiece.number = '0';

            this.hand1_nextPiece = curBoard[hand1NextMove];
            this.hand2_nextPiece = curBoard[hand2NextMove];

            this.firstMove = false;

            this.hand1_colliding = false;
            this.hand2_colliding = false;

            this.secondMove = true;
        }
        else {
            if(Math.floor(this.hand1.angle) !== this.hand1_nextPiece.angle)
                this.hand1.angle -= 1;

            if(Math.floor(this.hand2.angle) !== this.hand2_nextPiece.angle)
                this.hand2.angle += 1;
        }
    }
    else if(this.secondMove) {
        if(Math.floor(this.hand1.angle) === this.hand1_nextPiece.angle && Math.floor(this.hand2.angle) === this.hand2_nextPiece.angle) {
            this.hand1_nextPiece.valid = true;
            this.hand2_nextPiece.valid = true;

            // Checking for a win
            var curBoard = this.hand1_nextPiece.board;
            var won = true;

            for(var i = 0; i < curBoard.length; ++i) {
                if(curBoard[i].number != '0')
                    won = false;
            }

            if(won)
                isWon = true;
            else if(!won && this.hand1_nextPiece.number === '0' && this.hand2_nextPiece.number === '0')
                isLost = true;

            //this.hand1_nextPiece.numberText.bringToTop();
            //this.hand2_nextPiece.numberText.bringToTop();

            // End of second move
            this.secondMove = false;
        }
        else {
            if(Math.floor(this.hand1.angle) !== this.hand1_nextPiece.angle)
                this.hand1.angle += 1;

            if(Math.floor(this.hand2.angle) !== this.hand2_nextPiece.angle)
                this.hand2.angle -= 1;
        }
    }
}
