ClockPiece = function(x, y, board, hands, pos, number) {
    this.sprite = game.add.sprite(x, y, 'piece');

    // this.numberText.font = 'Trade Winds';
    // this.numberText.fill = '#FFFFFF';
    // this.numberText.align = 'center';

    this.hand1 = game.add.sprite(this.sprite.x, this.sprite.y, 'piece_hand');
    this.hand1.anchor.setTo(0.5, 0);
    this.hand2 = game.add.sprite(this.sprite.x, this.sprite.y, 'piece_hand');
    this.hand2.anchor.setTo(0.5, 0);

    this.hand1.angle = (Math.floor(Math.random() * 180)) + 1;
    this.hand1.angle *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;

    this.hand2.angle = (Math.floor(Math.random() * 180)) + 1;
    this.hand2.angle *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;

    this.numberText = game.add.text(this.sprite.x, this.sprite.y, number);
    this.numberText.font = 'Trade Winds';
    this.numberText.fill = '#FFFFFF';
    this.numberText.align = 'center';
    this.numberText.size = '32';
    this.numberText.anchor.setTo(0.5);

    this.valid = true;
    this.selected = false;
    this.number = number.toString();
    this.board = board;
    this.hands = hands;
    this.position = pos;

    this.sprite.anchor.set(0.5);
    this.sprite.inputEnabled = true;
    this.sprite.events.onInputOver.add(this.mouseOver, this);
    this.sprite.events.onInputOut.add(this.mouseOut, this);
    this.sprite.events.onInputDown.add(this.mouseDown, this);

    game.physics.enable(this.sprite, Phaser.Physics.ARCADE);

    // Find angle of sprite
    var pieceCenterX = this.sprite.x;
    var pieceCenterY = this.sprite.y;

    var dx = pieceCenterX - this.hands.hand1.x;
    var dy = pieceCenterY - this.hands.hand1.y;

    this.angle = -(Math.floor(Math.atan2(dx, dy) * (180 / Math.PI)));

    // HERE COME THE HAX
    if(this.angle == 180)
        this.angle = 179;
}

ClockPiece.prototype.setSprite = function(key) {
    //this.sprite.loadTexture(key);
    //this.key = key;
    //this.number = key.substring(0, 1);
}

ClockPiece.prototype.mouseOver = function() {
    /*
    if(this.key == this.number || this.key == this.number + '_valid') {
        this.setSprite(this.number + '_selected');
    }
    */

    this.selected = true;
}

ClockPiece.prototype.mouseOut = function () {
    /*
    if(this.key == this.number + '_selected') {
        if(!this.valid)
            this.setSprite(this.number);
        else
            this.setSprite(this.number + '_valid');
    }
    */

    this.selected = false;
}

ClockPiece.prototype.mouseDown = function() {
    if(this.valid && !isMoving && this.number != '0') {
        for(var i = 0; i < this.board.length; ++i) {
            var piece = this.board[i];
            piece.valid = false;
            //piece.setSprite(piece.number);
        }

        this.numberText.visible = false;
        this.hands.moveTo(this);
    }
}

ClockPiece.prototype.update = function() {
    if(this.selected && this.number !== '0') {
        this.hand1.angle += 2;
        this.hand2.angle -= 2;
    }
}
