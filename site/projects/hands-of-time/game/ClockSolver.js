ClockSolver = function() {
    console.log('Super Solve-O-Matic 3 Million');

    ClockSolver.prototype.generatePuzzle = function(size) {

    }

    ClockSolver.prototype.validatePuzzle = function(board) {

    }

    ClockSolver.prototype.testClock = function(board, index, chain) {
        // Who says you can't overload in JS :)
        if(typeof chain == 'undefined')
            chain = [];

        // If it is 0 then we don't do anything
        if(board[index] == 0)
            return chain;

        // Find locations of next position
        var nextPosition1 = index + board[index];
        var nextPosition2 = index - board[index];

        // Account for wraparound
        if(nextPosition1 >= board.length)
            nextPosition1 = nextPosition1 - board.length;
        if(nextPosition2 < 0)
            nextPosition2 = board.length - Math.abs(nextPosition2);

        board[index] = 0;
        chain.push(index);

        var solution = this.testClock(board, nextPosition1, chain);
        if(solution.length == board.length)
            return solution;

        return this.testClock(board, nextPosition2, chain);
    }
}

var boardValues = [2,3,2,3,3,3,1,4];

var solver = new ClockSolver();

//solver.validatePuzzle(boardValues);
