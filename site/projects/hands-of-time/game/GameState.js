var GameState = function(game) {};

//var boardValues = [2, 3, 3, 3, 1, 4, 2, 3];

// Yeah this is really shit, I know I Could do it better but it is currently
// 1:11 on the Friday of the tech expo. Sorry.
var boards = new Array();

boards[0] = new Array(3, 4, 5, 2, 3, 4, 5);
boards[1] = new Array(7, 4, 3, 5, 4, 6, 1, 3);
boards[2] = new Array(2, 4, 2, 1, 1, 3, 4);
boards[3] = new Array(5, 2, 3, 3, 7, 8, 2);
boards[4] = new Array(5, 2, 2, 1, 3, 3, 5);
boards[5] = new Array(3, 3, 4, 7, 5, 4, 2, 1);
boards[6] = new Array(9, 8, 7, 6, 5, 4, 2, 3);
boards[7] = new Array(6, 4, 3, 2, 5, 4, 4, 4, 3);

/*boards[0] = new Array(3, 3, 4, 7, 5, 4, 2, 1)*/

var board = [];

var hands;
var background;
var clockback;

var winText;
var winText_Set = false;

var loseText;
var loseText_Set = false;

var isMoving = false;
var isWon = false;
var isLost = false;

GameState.prototype = {
preload: function() {
    game.load.image('piece', 'assets/clockpiece.png');
    game.load.image('piece_hand', 'assets/clockpiece_hand.png');

    game.load.image('clockback', 'assets/clockback.png');

    game.load.image('hand1', 'assets/hourhand.png');
    game.load.image('hand2', 'assets/minutehand.png');
    game.load.image('starfield', 'assets/starfield.jpg');
},

create: function() {
    game.physics.startSystem(Phaser.Physics.ARCADE);

    background = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'starfield');

    clockback = game.add.sprite(0, 0, 'clockback');

    var radius = game.world.width / 2;

    // Create the hands of the clock
    hands = new ClockHands(radius, game.world.centerY);

    // Set needed values for computing piece position
    var pieceWidth = game.cache.getImage('piece').width;
    var pieceHeight = game.cache.getImage('piece').height;

    var boardValues = boards[Math.floor(Math.random() * (boards.length - 1))];

    // Compute the position for each piece and add it to the array
    for(var i = 0; i < boardValues.length; ++i) {
        // TRIGONOMETRY!1!11!!!1111!!11
        var x = game.world.centerX + ((radius - pieceWidth / 1.5) * Math.cos((2 * Math.PI) * (i / boardValues.length)));// + pieceWidth / 1.5;
        var y = game.world.centerY + ((radius - pieceHeight / 1.5) * Math.sin((2 * Math.PI) * (i / boardValues.length)));// + pieceHeight / 1.5;
        board.push(new ClockPiece(x, y, board, hands, i, boardValues[i]));
    }

    hands.hand1.bringToTop();
    hands.hand2.bringToTop();

    winText = game.add.text(game.world.centerX, game.world.centerY - 100, 'You win! Press F5 to restart');
    winText.font = 'Trade Winds';
    winText.fill = '#FFFFFF';
    winText.align = 'center';
    winText.anchor.set(0.5);
    winText.visible = false;

    loseText = game.add.text(game.world.centerX, game.world.centerY - 100, 'You lose! Press F5 to restart');
    loseText.font = 'Trade Winds';
    loseText.fill = '#FFFFFF';
    loseText.align = 'center';
    loseText.anchor.set(0.5);
    loseText.visible = false;
},

update: function() {
    if(isWon && winText_Set == false) {
        game.time.events.loop(Phaser.Timer.SECOND, this.updateWinText, null);
        winText_Set = true;
    }
    else if(isWon && winText_Set) {
        //if(game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
        //    game.state.start('title');
        //}
    }

    if(isLost && loseText_Set === false) {
        game.time.events.loop(Phaser.Timer.SECOND, this.updateLoseText, null);
        loseText_Set = true;
    }
    else if(isLost && loseText_Set) {

    }

    for(var i = 0; i < board.length; ++i)
        board[i].update();

    hands.update();
}

}

GameState.prototype.updateWinText = function() {
    if(winText.visible === true)
        winText.visible = false;
    else
        winText.visible = true;
}

GameState.prototype.updateLoseText = function() {
    if(loseText.visible === true)
        loseText.visible = false;
    else
        loseText.visible = true;
}

function mouseCollides(piece) {
    // COLLISION MAGIC
    return (
        (game.input.x < (piece.sprite.x + piece.sprite.width)) &&
        (game.input.y < (piece.sprite.y + piece.sprite.height)) &&
        (game.input.x > piece.sprite.x) &&
        (game.input.y > piece.sprite.y)
    );
}
