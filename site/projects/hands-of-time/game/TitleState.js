var TitleState = function(game) {};

var grandfatherHand_firstMove = true

TitleState.prototype = {

// Render Textures
texture1: undefined,
texture2: undefined,
texture3: undefined,
star: undefined,
stars: [],

grandfatherHand: undefined,
grandfatherHand_anchor: undefined,
titleBackground: undefined,
titleText: undefined,
startKey: undefined,

preload: function() {
    game.load.image('star', 'assets/star.png');
    game.load.image('titleBackground', 'assets/titleClock.png');
    game.load.image('grandfatherHand', 'assets/grandHand.png');
    console.log('Loaded title assets');
},

create: function() {
    // Render texture initialization
    star = game.make.sprite(0, 0, 'star');

    texture1 = game.add.renderTexture(game.world.width, game.world.height, 'texture1');
    texture2 = game.add.renderTexture(game.world.width, game.world.height, 'texture2');
    texture3 = game.add.renderTexture(game.world.width, game.world.height, 'texture3');

    game.add.sprite(0, 0, texture1);
    game.add.sprite(0, 0, texture2);
    game.add.sprite(0, 0, texture3);

    var curTexture = texture1;
    var curSpeed = 1;

    for(var i = 0; i < 300; ++i) {
        if(i == 100) {
            curSpeed = 2;
            curTexture = texture2;
        }
        else if(i == 200) {
            curSpeed = 3;
            curTexture = texture3;
        }
        this.stars.push({x: game.world.randomX, y: game.world.randomY, speed: curSpeed, texture: curTexture});
    }

    grandfatherHand = game.add.sprite(game.world.centerX, -15, 'grandfatherHand');
    grandfatherHand.anchor.set(0.5, 0);

    titleBackground = game.add.sprite(0, 0, 'titleBackground');

    startKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACE);

    titleText = game.add.text(game.world.centerX, 75, 'Hands of Time');
    titleText.anchor.set(0.5);
    titleText.font = 'Trade Winds';
    titleText.align = 'center';
    titleText.fill = '#FFFFFF';

    startText = game.add.text(game.world.centerX, game.world.height - 75, 'Press ENTER to Begin');
    startText.font = 'Trade Winds';
    startText.anchor.set(0.5);
    startText.align = 'center';
    startText.fill = '#FFFFFF';

    game.time.events.loop(Phaser.Timer.SECOND, this.updateStartText, null);

    console.log('Initialized title state');
},

update: function() {
    this.updateRenderTextures();

    if(grandfatherHand_firstMove) {
        if(grandfatherHand.angle >= 60) {
            grandfatherHand_firstMove = false;
        }
        else {
            grandfatherHand.angle += 1;
        }
    }
    if(!grandfatherHand_firstMove) {
        if(grandfatherHand.angle <= -60) {
            grandfatherHand_firstMove = true;
        }
        else {
            grandfatherHand.angle -= 1;
        }
    }

    if(game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
        grandfatherHand.angle -= 1;
    }
    else if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
        grandfatherHand.angle += 1;
    }

    if(game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
        game.state.start('game');
    }
}

}

TitleState.prototype.updateRenderTextures = function() {
    for(var i =0; i < 300; ++i) {
        this.stars[i].y += this.stars[i].speed;

        if(this.stars[i].y > game.world.height) {
            this.stars[i].x = game.world.randomX;
            this.stars[i].y = -32;
        }

        if(i == 0 || i == 100 || i == 200) {
            this.stars[i].texture.renderXY(star, this.stars[i].x, this.stars[i].y, true);
        }
        else {
            this.stars[i].texture.renderXY(star, this.stars[i].x, this.stars[i].y, false);
        }
    }
}

TitleState.prototype.updateStartText = function() {
    //startText.visible = (startText.visible) ? true : false;
    if(startText.visible)
        startText.visible = false;
    else
        startText.visible = true;
}
