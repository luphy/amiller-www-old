// Google Fonts Loader
WebFontConfig = {
    google: {
      families: ['Trade+Winds::latin']
    }
};

var game = new Phaser.Game(650, 650, Phaser.AUTO, 'game', {preload: preload, create: create, update: update, render: render});

function preload() {
    game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
    console.log('Loaded main assets');
}

function create() {
    console.log('Registering title state');
    game.state.add('title', TitleState);

    console.log('Registering main game state');
    game.state.add('game', GameState);

    console.log('Starting title state');
    game.state.start('title');
}

function update() {

}

function render() {

}
